#5 Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `data-source.ts` file
3. Run `npm start` command
4. Deal-with-Image.postman_collection.json 这个文件是postman用的，导入后可以看到一个命令，可以向后台发消息，模拟前台向后台发的登录api
5. 后台程序每次启动都会创建一次，在data-source.ts里synchronize: true,这个就是开关，创建好库表后，可以改成false,避免重复创建表
