import "reflect-metadata"
import {DataSource} from "typeorm";
import {User} from "./entities/User";
import {Images} from "./entities/Images";
const mysql = require('mysql2');

export const AppDataSource = new mysql.createConnection({
    host: 'localhost',
    port: +process.env.MYSQL_PORT,
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
})
export const InitDataSource = new DataSource({
    type: "mysql",
    host: "localhost",
    port: +process.env.MYSQL_PORT,
    username: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    synchronize: false,
    logging: true,
    entities: [User, Images],
    subscribers: [],
    migrations: [],
    cache: {
        type: "ioredis",
        duration: 60000,
        options: {
            host: "localhost",
            port: +process.env.REDIS_PORT,
        }
    },
    extra: {
        max: 10,
        connectionTimeoutMillis: 2000
    }
})
