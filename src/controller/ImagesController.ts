import {Request, Response} from "express";
import {Err, ErrStr, HttpCode} from "../helper/Err";
import {InitDataSource} from "../data-source";
import {Images} from "../entities/Images";
import {isPic, createImgConfig, ImgConfig, backUpImg, convertToWebp, UpdateImages} from "./pressing"

const repo = InitDataSource.getRepository(Images);


export async function ImageUpdate(request, response) {
    console.log('ImageUpdate')
    let config: any = [];
    if (!InitDataSource.isInitialized) {
        await InitDataSource.initialize();
    }
const {source,LoginData,CuttingState,WaterMarkState}=request.body.formData
    console.log(source,LoginData,CuttingState,WaterMarkState,'----')
    try {
        for (const key in source) {

                let compressed: any = await UpdateImages(source[key],LoginData,CuttingState,WaterMarkState);
                console.log('----压缩图片结束----',compressed);

                let image = new Images();
                image.Cutting = compressed.Cutting;
                image.WaterMark = compressed.WaterMark;

                let uploadDB: any = await repo.save(compressed);
                console.log(uploadDB,'数据格式');
                config.push(uploadDB);

        } //end of for loop
    } catch (error) {
        return response.status(500).json(error);
    }
    return response.status(200).send(config)
}
export async function ImagesMakeOne(request: Request, response: Response) {
    let images = null
    try {
        images = await repo.find({
            // where: {imageId: imageId},
            cache: true,
        })
    } catch (e) {
        console.log("can't find the user", e)
        return response.status(400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
    }
    return response.status(200).send(new Err(200, ErrStr.OK, images))
}

export async function ImagesMakeAll(request: Request, response: Response) {
    console.log('get all images')
    let images = []
    try {
        images = await repo.find()
    } catch (e) {
        console.log("save db error :", e)
        return response.status(400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
    }
    return response.status(200).send(images)
}

export async function ImageMaker(request, response) {
    const {email} = request.params
    let config: any = [];
    let backed: any;
    if (!InitDataSource.isInitialized) {
        await InitDataSource.initialize();
    }
    try {
        for (const key in request.files) {
            if (isPic(request.files[key].mimetype)) {
                // console.log(request.files[key],'--11--')
                const temp: ImgConfig = createImgConfig(request.files[key].path);
                temp.picExt = (request.files[key].mimetype);
                backed = await backUpImg(request.files[key], temp);
                console.log('备份图片结束',backed)
                let compressed: any = await convertToWebp(backed);
                console.log('----压缩图片结束----',compressed)
                let image = new Images();
                image.imageURL = compressed.compressFiles;
                image.imageURLOld = compressed.backUpOrgPath;
                image.imageId= compressed.backUpOrg;
                image.imageTHUM=compressed.ThumbnailFiles;
                image.imageMark =compressed.markFiles;
                image.thumbnail=true;
                image.user=email;
                let uploadDB: any = await repo.save(image);
                console.log(uploadDB,email,'数据格式');
                config.push(uploadDB);
            } else {
                return response.status(400).json({Message: "please select a image in correct format."});
            }
        } //end of for loop
    } catch (error) {
        return response.status(500).json(error);
    }
    return response.status(200).send(config)
}
export async function DeleteOneImages(request: Request, response: Response) {
    const {Id} = request.params
    console.log('delete one images id =',Id)

    try {
       let images = await repo.delete(Id)
        console.log(images)
        console.log('----')
    } catch (e) {
        console.log("save db error :", e)
        return response.status(400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
    }
    return response.status(200).send(HttpCode.E200)
}
