import {NextFunction, Request, Response} from "express";
import {User} from "../entities/User";
import {Err, ErrStr, HttpCode} from "../helper/Err";
import {InitDataSource} from "../data-source";

const sharp = require('sharp');
const repo = InitDataSource.getRepository(User)

export async function login(request: Request, response: Response) {
    if (!InitDataSource.isInitialized) {
        await InitDataSource.initialize();
    }

    // if(request.body) return  console.log(request.body);
    let {email, password} = request.body
    console.log(typeof email, email, typeof password, password);
    let user = null
    try {
        user = await repo.findOne({
            where: {email: email, password: password},
            cache: true,
        })
    } catch (e) {
        console.log("can't find the user", e)
        return response.status(400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
    }
    return response.status(200).send(new Err(HttpCode.E200, ErrStr.OK, user))
}

export async function signup(request: Request, response: Response) {
    console.log('signup', request.body)
    let UserBackData: any = [];
    let {SignupData} = request.body
    let {email, password, firstName, lastName} = SignupData
    let user = new User()
    user.email = email;
    user.password = password;
    user.firstName = firstName;
    user.lastName = lastName;
    try {
        UserBackData = await repo.save(user)
    } catch (e) {
        console.log("save db error :", e)
        return response.status(400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
    }
    return response.status(200).send(new Err(HttpCode.E200, ErrStr.OK, UserBackData))
}

export async function updateUser(request: Request, response: Response) {
    let UserUpdateData: any = [];
    let {email, ...otherData} = request.body.updateData
    console.log('update', otherData, email, typeof otherData.phone)
    try {
        await repo.update(email, otherData)
        UserUpdateData = await repo.findOne({
            where: {email: email},
            cache: true,
        })
    } catch (e) {
        return response.status(400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
    }
    return response.status(200).send(new Err(HttpCode.E200, ErrStr.OK, UserUpdateData))
}

export async function addTextOnImage(request: Request, response: Response, next: NextFunction) {
    try {
        const width = 260;
        const height = 70;
        const text = "WaterMark";

        const svgImage = `
    <svg width="${width}" height="${height}">
      <style>
      .title { fill: #001; font-size: 50px; font-weight: bold;}
      </style>
      <text x="50%" y="50%" text-anchor="middle" class="title">${text}</text>
    </svg>
    `;
        const svgBuffer = Buffer.from(svgImage);
        const image = await sharp(svgBuffer).toFile("c:/fullstack/public/mark.png");
        console.log(image)
        next();
    } catch (error) {
        console.log(error);
    }
}
