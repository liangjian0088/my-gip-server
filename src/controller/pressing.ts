import * as fileSystem from "fs";
import * as fs from "fs";

const sharp = require('sharp');

const allowedTypes = ['image/png', 'image/jpeg', 'image/jpg', 'image/gif', 'image/webp', 'image/svg+xml'];


export const UpdateImages = async (oneFile, LoginData, CuttingState, WaterMarkState) => {

    const tailorOBJ = JSON.parse(LoginData.tailor)
    console.log(typeof tailorOBJ, tailorOBJ, oneFile)
    try {
        if (oneFile.Cutting === false && CuttingState === true) {

            await sharp(oneFile.imageURLOld)
                .extract({width: tailorOBJ.width, height: tailorOBJ.height, left: tailorOBJ.x, top: tailorOBJ.y})
                .jpeg({force: true, quality: 80, effort: 6})
                .toFile(oneFile.imageURL);
            console.log('裁剪处理')
            oneFile.Cutting = true;
        }
        if (oneFile.WaterMark === false && WaterMarkState === true) {

            const image = await sharp(oneFile.imageURL)
                .composite([
                    {
                        input: 'c:/fullstack/public/mark.png',
                        top: 0,
                        left: 0,
                    },
                ])
                .toFile(oneFile.imageMark);
            console.log('增加水印',image)
            oneFile.WaterMark = true;
        }
        // fs.unlinkSync(oneFile.path);
        return oneFile
    } catch (e) {
        console.error(e);
    }
};
export const convertToWebp = async (oneFile) => {
    const isImage = allowedTypes.includes(oneFile.picExt);
    const isGif = oneFile.picExt === 'image/gif';
    oneFile.compressFiles = isImage && !isGif ? oneFile.compressFiles.replace(/.[^.]+$/, '.jpg') : oneFile.backUpOrgPath;
    oneFile.ThumbnailFiles = isImage && !isGif ? oneFile.ThumbnailFiles.replace(/.[^.]+$/, '.jpg') : oneFile.backUpOrgPath;
    if (oneFile.picExt !== 'image/webp' && !isGif && isImage) {
        try {

            await sharp(oneFile.backUpOrgPath)
                .resize({
                    width: 150,
                    height: 97
                })
                .toFormat("jpg")
                .jpeg({force: true, quality: 80, effort: 6})
                .toFile(oneFile.ThumbnailFiles);
            fs.unlinkSync(oneFile.path);
            return oneFile
        } catch (e) {
            console.error(e);
        }
    }
};

export interface ImgConfig {
    path: string;
    picExt: string;
    quality: number;
    backUpOrg: string;
    backUpOrgPath: string;
    ThumbnailFiles: string;
    compressFiles: string;
    markFiles:string;
}


export function isPic(image: string) {
    const ext: string = image.toLowerCase();
    return (ext == 'image/jpg' || ext == 'image/png' || ext == 'image/svg' || ext == 'image/gif' || ext == 'image/jpeg' || ext == 'image/svg');
}

export function createImgConfig(path: string): ImgConfig {
    return {
        path: path,         //'/fetchFunctionEnum'
        picExt: '',           //['png', 'jpg', 'jpeg', 'gif', 'bmp', 'svg']
        quality: 1.0,          //0.1-1.0, default is 1.0
        backUpOrg: '',        //default is ''
        backUpOrgPath: 'backUpOrg',  //default is backUpOrg
        ThumbnailFiles: 'thumbnail',        //default is thumbnail
        compressFiles: 'compressFiles',  //default is compressFiles
        markFiles: 'markFiles',  //default is compressFiles
    }
}

export function backUpImg(file: any, temp: any) {
    return new Promise((resolve, reject) => {
        fileSystem.readFile(file.path, function (error, data) {
            if (!file) {
                reject('file is null');
            }
            const filePath = process.env.OldPath  + file.originalname;
            const newFilePath = process.env.PurposePath  + file.originalname;
            const ThumbnailFiles = process.env.PurposePath  + 't' + "-" + file.originalname;
            const MarkFiles=process.env.PurposePath  + 'w' + "-" + file.originalname;
            fileSystem.writeFile(filePath, data, function (error) {
                if (error) {
                    reject(error);
                }
                temp.backUpOrgPath = filePath;
                temp.backUpOrg = file.originalname;
                temp.compressFiles = newFilePath;
                temp.ThumbnailFiles = ThumbnailFiles;
                temp.markFiles = MarkFiles;
                resolve(temp);
            })
        })
    })
}

