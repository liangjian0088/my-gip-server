import {Router} from "express";
import {DeleteOneImages, ImageMaker, ImagesMakeAll, ImagesMakeOne, ImageUpdate} from "../controller/ImagesController";
const router=Router()
router.get('/',ImagesMakeOne)
router.post('/:email',ImageMaker)
router.post('/',ImagesMakeAll)
router.delete('/:Id',DeleteOneImages)
router.put('/',ImageUpdate)

export default router
