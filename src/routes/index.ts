import {Router} from "express";
import user from "./user";
import images from "./images";

const routes = Router()
routes.use('/user',user)
routes.use('/images',images)
export default routes
