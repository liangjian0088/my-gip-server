import {Router} from "express";
import {addTextOnImage, login, signup, updateUser} from "../controller/UserController";

const router=Router()
router.post('/',login)
router.post('/signup/',signup)
router.put('/',updateUser)
router.put('/waterMark/',addTextOnImage,updateUser)
// router.delete('/:userId',UserController.delete)
export default router

