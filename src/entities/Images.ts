import {
    Column, CreateDateColumn,
    Entity, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn,
} from "typeorm";
import {User} from "./User"
@Entity()
export class Images {
    @PrimaryGeneratedColumn()
    id:number
    @Column()
    imageId: string;
    @Column({default: false})
    thumbnail: boolean;
    @Column({default: false})
    Cutting: boolean;
    @Column({default: false})
    WaterMark: boolean;
    @Column()
    imageMark: string;
    @Column()
    imageTHUM: string;
    @Column()
    imageURL: string;
    @Column()
    imageURLOld: string;
    @Column()
    @CreateDateColumn()
    createAt: Date;
    @Column()
    @UpdateDateColumn()
    updateAt: Date;
    @ManyToOne(() => User, user => user.images)
    user: User;
}
