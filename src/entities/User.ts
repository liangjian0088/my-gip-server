import {
    Column, CreateDateColumn,
    Entity, OneToMany, PrimaryColumn, Unique, UpdateDateColumn
} from "typeorm";
import {IsEmail, Length, Max, Min} from "class-validator";
import {Images} from "./Images";
import * as timers from "timers";
@Entity()
@Unique(['email'])
export class User {
    @PrimaryColumn()
    @IsEmail()
    @Length(5, 150)
    email: string;
    @Column({select:false})
    @Length(6, 200)
    password: string;
    @Column()
    @Length(1, 100)
    firstName: string;
    @Column()
    @Length(1, 100)
    lastName: string;
    @Column({nullable: true})
    @Min(15)
    @Max(120)
    age: number;

    @Column({nullable: true})
    phone: string;
    @Column({nullable: true})
    gender: string;
    @Column({nullable: true})
    birthday: Date;
    @Column({nullable: true})
    Watermark: string;
    @Column({nullable: true})
    WatermarkURL: string;
    @Column({nullable: true})
    WatermarkText: string;
    @Column({nullable: true})
    tailor: string;

    @Column({nullable: true})
    avatar: string;
    @Column()
    @CreateDateColumn()
    createAt: Date;
    @Column()
    @UpdateDateColumn()
    updateAt: Date;
    @OneToMany(() => Images, images => images.user,{eager:true})
    images: Images[]
}
