import {AppDataSource, InitDataSource} from "./data-source"
import * as express from "express";
import routes from "./routes/index";


export const app = express();
AppDataSource.connect(async function (err) {
    if (err) throw err;
    const multer  = require('multer');
    const upload = multer({ dest: process.env.SourcePath });
    app.use(upload.any());
    await InitDataSource.initialize();
    const cors = require("cors");
    app.use(cors());
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use('/', routes);
    app.listen(process.env.SERVER_PORT);
    console.log(`MYSQL Connected !!! Express server has started on port ${process.env.SERVER_PORT}. Open http://localhost:${process.env.SERVER_PORT} to see results`);
});

